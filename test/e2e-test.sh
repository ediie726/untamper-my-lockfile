#!/bin/bash

cmd=""
if [ "${CI:false}" = "true" ]; then
  cmd="untamper-my-lockfile"
  if ! command -v "$cmd" &> /dev/null; then
      echo "$cmd could not be found"
      exit 1
  fi
else
  SCRIPT_DIR=$(dirname "$(readlink -f "$0")")
  cmd="$SCRIPT_DIR/../bin/untamper.js"
  echo "Seemingly not running in CI, so falling back on $cmd"
fi

function check_file() {
  local file="$1"
  local exit_code="$2"
  local snapshot
  snapshot="$file.snapshot.txt"
  local result
  result="$file.result.txt"

  rm -f "$result"

  echo "Testing $file"
  "$cmd" --lockfile "$1" &> "$result"
  if [ "$?" -ne "$exit_code" ]; then
    echo -e "\tTEST FAILED. Expected $exit_code, retrieved $?"
    exit 1
  fi

  if ! cmp -s "$snapshot" "$result"; then
    echo -e "\tOutput between $snapshot and $result differs..."
    diff "$snapshot" "$result"
    exit 1
  fi

  echo -e "\tTest succeeded..."
}

check_file ./test/fixtures/unsupported/package-lock.json 1
check_file ./test/fixtures/unsupported/yarn.lock 1
check_file ./test/fixtures/tampered/yarn.lock 1
check_file ./test/fixtures/untampered/yarn.lock 0

echo 'TESTS SUCCEEDED'
exit 0
