#!/bin/sh
set -euo

DOCKER_IMAGE="${DOCKER_IMAGE:-untamper-my-lockfile}"

if [ "$CI" = "true" ]; then
    docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" "$CI_REGISTRY"
    if docker manifest inspect "$DOCKER_IMAGE" >/dev/null; then
        echo "Image already exists"
        docker pull "$DOCKER_IMAGE"
        label=$(docker inspect --format='{{.Config.Labels.commitRef}}' "$DOCKER_IMAGE")
        if [ "$label" = "$CI_COMMIT_SHORT_SHA" ]; then
            echo "Image has correct ref, so we skip building"
            exit 0
        fi
    fi
fi

echo "Building image"
tar -c -v -z -f "package.tar.gz" --exclude="*.spec.js" bin/ lib/ package.json yarn.lock
docker build . --tag "$DOCKER_IMAGE" --label commitRef="$CI_COMMIT_SHORT_SHA"
rm -f package.tar.gz

if [ "$CI" = "true" ]; then
    echo "Pushing image"
    docker push "$DOCKER_IMAGE"
fi
