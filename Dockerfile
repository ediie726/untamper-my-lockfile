FROM node:16.14.0-alpine

ADD package.tar.gz /package/

RUN cd /package/ && \
    yarn install --production --frozen-lockfile && \
    yarn cache clean && \
    ln -s /package/bin/untamper.js /usr/local/bin/untamper-my-lockfile && \
    untamper-my-lockfile --version
