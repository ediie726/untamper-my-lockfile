const npa = require('npm-package-arg')
const pkg = require('package-json')
const { setTimeout } = require('timers/promises')
const { objectComplement } = require('../utils')

const DEFAULT_RETRIES = 3
const MAX_ERROR_COUNT = 50

class MaxRetriesError extends Error {
  constructor (count, error) {
    super(`
More than ${count} errors occurred when calling the registry.
Likely the registry is down or there are some connection problems.
Last error that occurred: ${error.message}
${error.stack}
`)
    this.name = 'MaxRetriesError'
  }
}

class VersionNotFoundError extends Error {
  constructor (name, version) {
    super(`${name}@${version} not found`)
    this.name = 'VersionNotFoundError'
  }
}

class Registry {
  constructor (args = {}) {
    const {
      maxErrorCount = MAX_ERROR_COUNT,
      maxRetries = DEFAULT_RETRIES
    } = args
    this.packageCache = {}
    this.maxRetries = maxRetries
    this.maxErrorCount = maxErrorCount
    this.errors = 0
  }

  parsePackageName (fullname) {
    const { name, type, subSpec } = npa(fullname)
    if (type === 'alias') {
      return this.parsePackageName(subSpec.raw)
    }
    return name
  }

  async pkg (name) {
    return pkg(name, { allVersions: true })
  }

  async getPackageDataWithRetries (name, retries = this.maxRetries) {
    for (let i = 1; i <= retries; i += 1) {
      try {
        return await this.pkg(name)
      } catch (e) {
        this.errors += 1
        if (this.errors > this.maxErrorCount) {
          throw new MaxRetriesError(this.maxErrorCount, e)
        }
        if (i >= retries) {
          throw e
        }
        // Exponential backoff
        await setTimeout((2 ** (i - 1)) * 1000)
      }
    }
  }

  async getPackageData (name) {
    if (!this.packageCache[name]) {
      this.packageCache[name] = this.getPackageDataWithRetries(name, this.maxRetries)
    }
    return await this.packageCache[name]
  }

  async getRemotePackageVersionInfo (name, version) {
    const pkgData = await this.getPackageData(name)
    const allVersions = Object.keys(pkgData.versions)

    if (!allVersions.includes(version)) {
      throw new VersionNotFoundError(name, version)
    }

    const {
      dependencies: origDependencies,
      optionalDependencies = {},
      bundleDependencies: origBundledDependencies,
      ...rest
    } = pkgData.versions[version]

    return {
      // Apparently npm duplicates optional dependencies into the "normal" dependencies
      dependencies: Object.fromEntries(objectComplement(origDependencies, optionalDependencies)),
      optionalDependencies,
      // Normalize bundled dependencies to be an array and be called _bundledDependencies_
      bundledDependencies: Array.isArray(origBundledDependencies) ? origBundledDependencies : [],
      ...rest
    }
  }
}

Registry.MaxRetriesError = MaxRetriesError
Registry.VersionNotFoundError = VersionNotFoundError

module.exports = Registry
