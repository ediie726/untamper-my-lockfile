const fs = require('fs/promises')
const { basename } = require('path')
const { parse: parseYarnLockfile } = require('@yarnpkg/lockfile')

const checkYarnV1 = require('./lock-files/yarn-v1')
const { createError, logger } = require('./utils')

async function checkYarn (filePath) {
  let yarnFile
  try {
    yarnFile = await fs.readFile(filePath, 'utf8')
  } catch (e) {
    return [createError(`Could not read lockfile: ${e.message}`, filePath)]
  }

  if (yarnFile.match(/^__metadata:$/m)) {
    return [createError('No support for yarn lock files of version 2 or higher.', filePath)]
  }

  try {
    yarnFile = parseYarnLockfile(yarnFile, filePath)
  } catch (e) {
    return [createError(`Could not parse lockfile: ${e.message}`, filePath)]
  }

  if (yarnFile.type !== 'success') {
    return [createError('Parsed lock file has merge conflicts. Please fix them and try again.', filePath)]
  }

  return checkYarnV1(yarnFile.object, filePath)
}

/**
 * Moved this logic into a separate function, so that once we support
 * more lockfiles, we can abstract the file type detection here.
 */
async function checkLockFile (filePath) {
  const type = basename(filePath)

  switch (basename(filePath)) {
    case 'yarn.lock':
      return checkYarn(filePath)
    default:
      return [createError(`No support for lock files of type '${type}'`, filePath)]
  }
}

async function checkLockFiles (lockFiles) {
  const filesToCheck = [lockFiles].flat().sort()

  logger.log(`Checking whether lockfile(s) ${filesToCheck.join(', ')} have been tampered with`)

  const results = []

  for (const filePath of filesToCheck) {
    results.push(...await checkLockFile(filePath))
  }

  return results.flat()
}

module.exports = checkLockFiles
