const asyncPool = require('tiny-async-pool')
const { logger, createError, objectComplement } = require('../utils')
const Registry = require('../helpers/registry')

function _toSha1IntegrityString (shasum) {
  return 'sha1-' + Buffer.from(shasum, 'hex').toString('base64')
}

function checkDependencyEntry (type, yarnEntry, versionPackageData) {
  const { bundledDependencies } = versionPackageData

  return [
    objectComplement(yarnEntry[type], versionPackageData[type])
      .map(([packageName, version]) => `Lockfile has '${packageName}@${version}' in '${type}' while not listed at the registry.`),
    objectComplement(versionPackageData[type], yarnEntry[type])
      .flatMap(([packageName, version]) => {
        /**
         * There are three ways bundled dependencies can interact with a yarn.lock
         * a.) package.json { dependencies: { example : '1.0.0'}, bundleDependencies: ['example']}
         *    => In this case the bundled dependency _will_ appear in the yarn.lock dependencies
         * b.) package.json { dependencies: { example : '1.0.0'}, bundleDependencies: ['sub_dependency_of_example']}
         *    1. => In this case the bundled dependency _might_ appear in the yarn.lock dependencies as sub_dependency_of_example: '*'
         *    2. => In this case the bundled dependency _might_ not appear in the yarn.lock dependencies
         */
        if (bundledDependencies.includes(packageName)) {
          return []
        }
        return `Lockfile is missing '${packageName}@${version}' in '${type}'.`
      })
  ].flat()
}

async function checkPackage (yarnEntry, filePath, registry) {
  const { version, resolved, integrity, allPatterns } = yarnEntry
  const name = registry.parsePackageName(allPatterns[0])

  const logName = `${name}@${version}`
  let pkgData

  try {
    pkgData = await registry.getRemotePackageVersionInfo(name, version)
  } catch (e) {
    if (e instanceof Registry.VersionNotFoundError) {
      return createError(`${logName}: version '${version}' doesn't exist in registry`, filePath)
    } else {
      throw e
    }
  }

  const { dist, ...versionPackageData } = pkgData
  const messages = []

  for (const type of ['dependencies', 'optionalDependencies']) {
    messages.push(
      ...checkDependencyEntry(type, yarnEntry, versionPackageData)
        .map(x => createError(`${logName}: ${x}`, filePath))
    )
  }

  // expectedIntegrity is a SHA512 hash of the package's tarball. If the SHA512 is not
  // present, it will be a SHA1 hash. In both cases, the hash is base64 encoded.
  //
  // Example:
  // sha512-iRDPJKUPVEND7dHPO8rkbOnPpyDygcDFtWjpeWNCgy8WP2rXcxXL8TskReQl6OrB2G7+UJrags1q15Fudc7G6w==
  // sha1-s2nW+128E+7PUk+RsHD+7cNXzzQ=
  const expectedIntegrity = integrity.startsWith('sha512-') ? dist.integrity : _toSha1IntegrityString(dist.shasum)
  if (integrity !== expectedIntegrity) {
    messages.push(createError(`${logName}: integrity hash is ${integrity}, expected ${expectedIntegrity}`, filePath))
  }

  // 'yarn info' resolves packages to registry.npmjs.org and doesn't respect --registry flag
  // change the registry manually to yarn's registry.
  const expectedResolved = dist.tarball.replace(
    'https://registry.npmjs.org',
    'https://registry.yarnpkg.com') +
    '#' + dist.shasum

  if (expectedResolved !== resolved) {
    messages.push(createError(`${logName}: resolved URL is ${resolved}, expected ${expectedResolved}`, filePath))
  }

  return messages
}

/**
 * Deduplicate a parsed yarn object.
 *
 * For de-duplicated yarn entries, the parser adds actually two entries to the object:
 * ```
 * lodash@4.17.20, lodash@^4.17.20:
 *   version "4.17.20"
 *   resolved "<url>"
 *   integrity "<hash>"
 * ```
 *
 * The example above will present as a hash, where <obj> is identical for both entries.
 *
 * ```
 * {
 *   "lodash@4.17.20": <obj>,
 *   "lodash@^4.17.20": <obj>
 * }
 * ```
 *
 * This function returns an array with the unique Set of entries.
 * Each entry has the patterns pointing to it added.
 *
 * So in our example above the entry for both lodash patterns would
 * look like this:
 *
 * ```
 * {
 *     version: '4.17.20',
 *     resolved: '<url>',
 *     integrity: '<integrity-hash>',
 *     allPatterns: [ 'lodash@4.17.20', 'lodash@^4.17.20' ]
 * }
 * ```
 */
function deduplicatedYarnEntries (yarnObject) {
  const acc = new Set()

  for (const [pattern, yarnEntry] of Object.entries(yarnObject)) {
    yarnEntry.allPatterns ||= []
    yarnEntry.allPatterns.push(pattern)
    acc.add(yarnEntry)
  }

  return [...acc]
}

async function checkYarnV1 (yarnObject, filePath) {
  const entries = deduplicatedYarnEntries(yarnObject)

  const registry = new Registry()

  const processPackage = async (yarnEntry) => {
    const { allPatterns } = yarnEntry
    logger.debug('checking entry: %s', allPatterns.join(', '))
    logger.logProgress(`Checking ${filePath}: package {count} of ${entries.length}`, false)
    try {
      return await checkPackage(yarnEntry, filePath, registry)
    } catch (e) {
      if (e instanceof Registry.MaxRetriesError) {
        throw e
      }
      return (createError(`Could not process entry for ${allPatterns.join(', ')}: ${e.message}`, filePath))
    }
  }

  let allMessages

  try {
    allMessages = (await asyncPool(10, entries, processPackage)).flat()
  } catch (e) {
    if (e instanceof Registry.MaxRetriesError) {
      logger.logProgress('', true)
      return [createError(e.message, filePath)]
    }
  }

  logger.logProgress(`Checking ${filePath}: package ${entries.length} of ${entries.length}`, true)

  return allMessages
}

module.exports = checkYarnV1
