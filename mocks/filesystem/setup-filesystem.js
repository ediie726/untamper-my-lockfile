const fs = require('fs/promises')

const sinon = require('sinon')

let mock = null

const fakeFiles = new Map()

const mockFile = (path, content) => {
  fakeFiles.set(path, content)
}

const clearMockFS = () => {
  if (mock) {
    mock.restore()
  }
  fakeFiles.clear()
}

const setupMockFS = () => {
  if (mock) {
    clearMockFS()
  }

  mock = sinon.stub(fs, 'readFile').callsFake(function (file) {
    if (fakeFiles.has(file)) {
      return fakeFiles.get(file)
    }
    throw new Error(`Unknown file ${file}`)
  })
}

module.exports = {
  setupMockFS,
  clearMockFS,
  mockFile
}
