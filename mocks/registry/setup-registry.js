const path = require('path')
const fs = require('fs')

const nock = require('nock')

let mock = null

const clearMockRegistry = () => {
  if (mock) {
    nock.cleanAll()
  }
}

const setupMockRegistry = (registry = 'https://registry.yarnpkg.com') => {
  if (mock) {
    clearMockRegistry()
  }

  mock = nock(registry)
    .persist()
    .get(/.+/)
    .reply((uri) => {
      try {
        return [
          200,
          JSON.parse(fs.readFileSync(path.join(__dirname, 'package-mocks', `${decodeURIComponent(uri)}.json`), 'utf8'))
        ]
      } catch (e) {
        return [
          404,
          `Package not found: ${e.message}`
        ]
      }
    })
}

module.exports = {
  setupMockRegistry,
  clearMockRegistry
}
